import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { AppLoading,Font } from "expo";
import { Provider } from 'react-redux';
import { createStore, applyMiddleware  } from 'redux'; //'helper methods'
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers';
import { Root } from "native-base";
import { StackNavigator } from "react-navigation";
import LoginForm from './src/components/LoginForm';
import ReceiptList from './src/components/ReceiptList';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

const AppNavigator = StackNavigator(
   { 
      LoginForm: { screen: LoginForm },
      ReceiptList: { screen: ReceiptList },
   },
   {
     initialRouteName: "LoginForm",
     headerMode: "none"
   }
 );
   

class App extends Component {

   state = {
      fontLoaded: false
   };

   async componentWillMount() {
      try {
         await Font.loadAsync({
            Roboto: require("./node_modules/native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("./node_modules/native-base/Fonts/Roboto_medium.ttf")
         });
         this.setState({ fontLoaded: true });
      } catch (error) {
        console.log('error loading icon fonts', error);
      }
    }

   render() {
      
      if (!this.state.fontLoaded) {
         return <AppLoading />;
      }

      //Este es el store de estados usado por redux
      const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

      return(
         <Provider store={ store }>
            <Root>
               <AppNavigator/>
            </Root>
         </Provider>
      );
   }
}

export default App;
