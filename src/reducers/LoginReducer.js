import { 
   CI_CHANGED,
   PART1_CHANGED,
   PART2_CHANGED,
   SUBMIT,
   SUBMIT_FAIL
} from '../actions/types';

// Estos valores iniciales son solo para referencia y para dar a entender cuales estados se estan usando en el reducer.
const INITIAL_STATE = { 
   cedula: '',
   part1: '',
   part2: '',
   loading: false
}; 

export default (state = INITIAL_STATE, action) => {
   
   console.log(action.type);
   switch(action.type) {
      
      case CI_CHANGED:

         //se utiliza el ...state, para copiar el objeto y sobreescribir el campo email,
         //con el nuevo valor, para que cuando redux compare internamente si el objeto cambio,
         //se encuentre que si xq le sobrescribimos un valor y cambie el state.
         return { ...state, cedula: action.payload };

      case PART1_CHANGED:
         return { ...state, part1: action.payload };

      case PART2_CHANGED:
         return { ...state, part2: action.payload };

      case SUBMIT:
         return { ...state, loading: true };

      case SUBMIT_FAIL:
         return { ...state, loading: false };

      default: 
         return state;
   }
};

