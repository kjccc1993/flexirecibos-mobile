import { combineReducers } from 'redux';
import LoginReducer from './LoginReducer';


//Los reducers recalculan los nuevos estados del app.
//Verificando que haya habido un cambio real en el estado.
//Luego se envian a cada uno de los componentes implicados para que se 
//re-rendericen con el nuevo stado.
export default combineReducers({
   login: LoginReducer
});