//Este archivo contiene todas las Escenas 'Scene' de nuestra app.
import React from 'react';
import { StatusBar } from 'react-native';
import { Stack, Scene, Router, Actions } from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import ReceiptList from './components/ReceiptList';

const RouterComponent = () => {

   const { navBarStylesCommon } = styles;
   
   return (
      <Router>
         <Stack key="root">
            <Scene
               key="login" 
               component={ LoginForm }
               hideNavBar
            /> 

            <Scene 
               key="receiptlist" 
               component={ ReceiptList }
               hideNavBar
            /> 
         </Stack>
      </Router>
   );

};

const styles = {
   navBarStylesCommon: {
      marginTop: StatusBar.currentHeight
   }
}

export default RouterComponent;
