import { Actions } from 'react-native-router-flux';
import { 
   CI_CHANGED,
   PART1_CHANGED,
   PART2_CHANGED,
   SUBMIT,
   SUBMIT_FAIL
} from './types';

//Las acciones son funciones que retornan objetos planos de js.
//Tienen un type y payload.
//Action creator
export const ciChanged = (text) => {
   return {
      type: CI_CHANGED,
      payload: text
   }
};

export const part1Changed = (text) => {
   return {
      type: PART1_CHANGED,
      payload: text
   }
};

export const part2Changed = (text) => {
   return {
      type: PART2_CHANGED,
      payload: text
   }
};

export const submit = ({ cedula, part1, part2, navigation }) => {
   return (dispatch) => {

      dispatch({ type: SUBMIT });

      setTimeout(()=>{
         dispatch({ type: SUBMIT_FAIL });
      }, 2000)
      
      navigation.navigate("ReceiptList");
   };
};

export const submitFail = () => {
   return (dispatch) => {

      setTimeout(()=>{
         dispatch({ type: SUBMIT_FAIL });
      }, 1000)
      
   };
};

