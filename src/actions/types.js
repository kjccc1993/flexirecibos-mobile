export const CI_CHANGED    = 'ci_changed';
export const PART1_CHANGED = 'part1_changed';
export const PART2_CHANGED = 'part2_changed';
export const SUBMIT        = 'submit';
export const SUBMIT_FAIL  = 'submit_fail';
