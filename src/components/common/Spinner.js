//Import libraries for making a component
import React from 'react';
import { View, ActivityIndicator } from 'react-native';

// Make a component
// Este es un componente funcional, para manejar datos estaticos.
const Spinner = ({ size })=> {

    return (
        <View style={styles.spinnerStyle}>
            <ActivityIndicator size={size || 'large'}  />
        </View>
    );
};

//Styles
const styles = {
    spinnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
}

// Make the component available to another parts of the app
export { Spinner };
