//Imports
import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

//Make a component.
const Button = (props) => {
    
    const { buttonStyle, textStyle } = styles;

    return (
        <TouchableOpacity onPress={props.onPress} style={buttonStyle}>
            <Text style={textStyle}>
                {props.children}
            </Text>
        </TouchableOpacity>
    );
};

//Styles
const styles = {
    textStyle: {
        alignSelf: 'center',
        color: '#fff',
        fontSize: 16,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10
    },
    buttonStyle: {
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#1dc5a3',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#1dc5a3',
        marginLeft: 5,
        marginRight: 5
    }
};

 
//Export the component
export { Button };
