//Import libraries for making a component
import React from 'react';
import { Text, View } from 'react-native';

// Make a component
// Este es un componente funcional, para manejar datos estaticos.
const Header = (props)=> {

    const { textStyle, viewStyle } = styles;

    return (
        <View style={ viewStyle }>
            <Text style={ textStyle }> { props.headerText || "" } </Text>
        </View>
    );
};

//Styles
const styles = {
    viewStyle: {
        backgroundColor: '#F8F8F8',
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        paddingTop: 18,
        elevation: 1,
        borderBottomColor: '#E8E8E8',
        borderBottomWidth: 1,
        position: 'relative'
    },
    textStyle: {
        fontSize: 20
    }
}

// Make the component available to another parts of the app
export { Header };
