//Imports
import React from 'react';
import { View, Dimensions } from 'react-native';

//Make a component.
const Card = (props) => {
   return (
      <View style={[styles.containerStyle, props.style]}>
         { props.children }
      </View>
   );
};

//Styles
const styles = {
    containerStyle: {
        borderColor: '#DDD',
        borderBottomWidth: 0,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10
    }
}

 
//Export the component
export { Card };