//Imports
import React from 'react';
import { View } from 'react-native';

//Make a component.
const CardSection = (props) => {
    
    return (
        <View style={[styles.containerStyle, props.style]}>
            { props.children }
        </View>
    );
};

//Styles
const styles = {
    containerStyle: {
        padding: 5,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        position: 'relative'
    }
}

 
//Export the component
export { CardSection };