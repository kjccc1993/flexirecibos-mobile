
//Para exportar componentes comunes desde un solo archivo,
//cada componente debe ser exportado como un objeto y no como default.
export * from './Button';
export * from './Card';
export * from './CardSection';
export * from './Header';
export * from './Input';
export * from './Spinner';