import React, { Component } from 'react';
import { 
   View, 
   StatusBar, 
   Image, 
   Dimensions, 
   ScrollView,
   KeyboardAvoidingView,
   Platform
} from 'react-native';
import { connect } from 'react-redux';
import { ciChanged, part1Changed, part2Changed, submit } from '../actions';
import {
   Container,
   Content,
   Button,
   Item,
   Label,
   Input,
   Form,
   Text,
   Spinner,
   Body,
   Header,
   Left,
   Right,
   Title
 } from "native-base";
 import { Grid, Row, Col } from "react-native-easy-grid";

 import { Ionicons } from '@expo/vector-icons';

class LoginForm extends Component {

   onSubmitPress(){
      const { cedula, part1, part2 } = this.props;
      console.log("BOTON PRESIONADO");
      this.props.submit(this.props);
   }

   onCIChange(text)
   {
      this.props.ciChanged(text); //Action creator
   }

   onPart1Change(text)
   {
      this.props.part1Changed(text); //Action creator
   }

   onPart2Change(text)
   {
      this.props.part2Changed(text); //Action creator
   }

   renderButton() {
      if(this.props.loading)
      {
         return <Spinner color="#1ab394" />;
      } 

      return (
         <Button 
            onPress={this.onSubmitPress.bind(this)} 
            block 
            style={{ marginLeft:15, marginTop: 50, backgroundColor: '#1ab394' }}
         >
            <Text color="white">Ver recibos</Text>
         </Button>
      );
   }

   renderForm() {
      return (
         <Container style={styles.container}>
            <View style={imgContainerStyle}>
               <Image
                  style={{ resizeMode: 'contain', width: width*0.7, marginTop: height*0.1 }}
                  source={require('../../res/img/flexiLogo-min.png')}
               />
            </View>

            <View 
               style={{ justifyContent: 'center', alignItems: 'center' }}
            >
               <Text style={{fontWeight: 'bold'}}>
                  Bienvenido a Flexipos/Recibo
               </Text>

               <Text style={{paddingTop: 10}}>Coloque los datos de la tarjeta con que realizó la </Text>
               <Text>compra y su número de cédula de identidad.</Text>
            </View>

            <Content style={contentStyle}>
               <Form>
                  <Item floatingLabel>
                     <Label>Cédula</Label>
                     <Input 
                        value={this.props.cedula}
                        onChangeText={this.onCIChange.bind(this)}
                        keyboardType='numeric'
                        maxLength={10}
                     />
                  </Item>
                  <Item floatingLabel>
                     <Label>Primeros 6 dígitos</Label>
                     <Input 
                        value={this.props.cedula}
                        onChangeText={this.onPart1Change.bind(this)}
                        keyboardType='numeric'
                        maxLength={6}
                     />
                  </Item>
                  <Item floatingLabel>
                     <Label>Últimos 4 dígitos</Label>
                     <Input 
                        onChangeText={this.onPart2Change.bind(this)}
                        value={this.props.part2}
                        keyboardType='numeric'
                        maxLength={4}
                     />
                  </Item>
               </Form>

               { this.renderButton() }
               
            </Content>
         </Container>
      );
   }

   render(){

      const {imgContainerStyle, titleStyle, inputStyle} = styles;
      const {height, width} = Dimensions.get('window');

      console.log(height, width);

      var contentStyle = {};
      contentStyle.width = width > 600 ? '70%' : '90%';

      // if(Platform.OS === "ios") {
      //    return (
      //       {renderForm()}
      //    );
      // }

      return (      
         <ScrollView>
            <KeyboardAvoidingView>
               <Container style={{paddingTop: StatusBar.currentHeight}}>
                  <Grid>
                     <Row size={30} style={imgContainerStyle}>
                        <Image
                           style={{ resizeMode: 'contain', width: "70%" }}
                           source={require('../../res/img/flexiLogo-min.png')}
                        />
                     </Row>
                     <Row size={15} style={{ justifyContent: 'center' }}>
                        <View 
                           style={{ justifyContent: 'center', alignItems: 'center' }}
                        >
                           <Text style={{fontWeight: 'bold'}}>
                              Bienvenido a Flexipos/Recibo
                           </Text>

                           <Text style={{paddingTop: 10}}>Coloque los datos de la tarjeta con que realizó la </Text>
                           <Text>compra y su número de cédula de identidad.</Text>
                        </View>
                     </Row>
                     <Row size={55}>
                        <Col size={5}/>
                        <Col size={90}>
                           <Content>
                              <Form>
                                 <Item floatingLabel>
                                    <Label>Cédula</Label>
                                    <Input 
                                       value={this.props.cedula}
                                       onChangeText={this.onCIChange.bind(this)}
                                       keyboardType='numeric'
                                       maxLength={10}
                                    />
                                 </Item>
                                 <Item floatingLabel>
                                    <Label>Primeros 6 dígitos</Label>
                                    <Input 
                                       value={this.props.part1}
                                       onChangeText={this.onPart1Change.bind(this)}
                                       keyboardType='numeric'
                                       maxLength={6}
                                    />
                                 </Item>
                                 <Item floatingLabel>
                                    <Label>Últimos 4 dígitos</Label>
                                    <Input 
                                       onChangeText={this.onPart2Change.bind(this)}
                                       value={this.props.part2}
                                       keyboardType='numeric'
                                       maxLength={4}
                                    />
                                 </Item>
                              </Form>
                              { this.renderButton() }
                           </Content>
                        </Col>
                        <Col size={5}/>
                     </Row>
                  </Grid>
               </Container>
            </KeyboardAvoidingView>
         </ScrollView>

      );
   }
}

const styles = {
   container: {
      backgroundColor: "#FFF"
    },
    imgContainerStyle:{
      justifyContent: 'center',
      alignItems: 'center'
   }
}

//Esta funcion permite obtener los estados que queremos desde nuestro archivo de reducers.
const mapStateToProps = state => {

   const { cedula, part1, part2, loading } = state.login;

   return{ cedula, part1, part2, loading };
}

//connect, permite conectar nuestra app con redux, se le envian el mapStateToProps y las acciones que queremos usar.
export default connect(mapStateToProps, {ciChanged, part1Changed, part2Changed, submit})(LoginForm);  