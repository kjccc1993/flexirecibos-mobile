import React, { Component } from 'react';
import { 
   View, 
   StatusBar, 
   Dimensions, 
   ScrollView,
   Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Card, CardSection } from './common';
import { Ionicons } from '@expo/vector-icons';
import {
   Container,
   Header,
   Title,
   Content,
   Button,
   Icon,
   Text,
   Left,
   Right,
   Body,
   List,
   ListItem
 } from "native-base";


class ReceiptList extends Component {


   onButtonPress() {
      console.log("PRESIONANDO")
      this.props.navigation.navigate("LoginForm");
   }

   render(){

      const {height, width} = Dimensions.get('window');
      const platform = Platform.OS;

      console.log(height, width);

      return (
         <Container style={{paddingTop: platform === "ios" ? 0 : StatusBar.currentHeight}}>
            <Header>
               <Left>
                  <Button transparent onPress={() => this.props.navigation.goBack()}>
                  <Ionicons name="ios-arrow-back" />
                  </Button>
               </Left>
               <Body>
                  <Title>123456******1234</Title>
               </Body>
               <Right />
            </Header>
            <Content>
               <List>
                  <ListItem 
                     button 
                     onPress={() => this.onButtonPress()}
                  >
                  <Text>Simon Mignolet</Text>
                  </ListItem >
                  <ListItem button>
                  <Text>Nathaniel Clyne</Text>
                  </ListItem>
                  <ListItem button>
                  <Text>Dejan Lovren</Text>
                  {/* <Right>
                        <Icon name="arrow-forward" />
                     </Right> */}
                  </ListItem>
               </List>
            </Content>
         </Container>
      );
   }
}

const styles = {


}

//Esta funcion permite obtener los estados que queremos desde nuestro archivo de reducers.
const mapStateToProps = state => {

   // const { cedula, part1, part2, loading } = state.login;

   // return{ cedula, part1, part2, loading };
}

//connect, permite conectar nuestra app con redux, se le envian el mapStateToProps y las acciones que queremos usar.
export default connect(null, null)(ReceiptList);  